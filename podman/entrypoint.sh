#!/usr/bin/sh

username="${1:?missing username}"

if ! id -u "$username" >/dev/null 2>&1; then
    echo "user '${username}' not found" >&2
    exit 1
fi

if (( $EUID != 0 )); then
    echo "please run as root" >&2
    exit 1
fi

# the 'wheel' group is a special group used to control access to the su or sudo command
usermod -a -G wheel $username

#su - $username -c bash
sudo -iu $username bash
