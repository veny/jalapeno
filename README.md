## Build image

```bash
podman build -t jalapeno-arch:220901 podman/
```

podman create --name FOO --network host --no-hosts --hostname PIPAPO \
  --ulimit=host --userns=keep-id --user root:root --interactive --tty \
  --volume /home/veny:/home/veny:rslave --volume /dev:/dev:rslave \
  --volume /home/veny/projects/jalapeno/podman/entrypoint.sh:/usr/bin/jalapeno.sh:ro \
  jalapeno-arch:220901 /usr/bin/jalapeno.sh $USER
# --dns none 


podman start -a FOO
#podman --log-level=debug start -a FOO
